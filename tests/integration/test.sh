#!/bin/sh

# test:
# - World_old.txt is deleted
# - World_new.txt is added
# - Hello.txt contect is updated
# test_multiPath:
# - multi disk/path backup works
# - World_old.txt is deleted
# - World_new.txt is added
# - Hello.txt contect is updated
# test_no-delete:
# - Hello_old.txt and World_old.txt are keept
# - Hello_new.txt and World_new.txt are created
# test_exclude:
# - no backup of exclude_folder
# - no backup of *.adoc files (nor in root folder nor in sub_folder)

folderPath="tests/integration"
testConfigFile="${folderPath}/testConfig.yml"
logFile="${folderPath}/test/test.log"

outputFileResult="${folderPath}/test/output.txt"
outputFileExpected="${folderPath}/test/output_expected.txt"

PY="python"

prepare_test() {
    rm -rf "${folderPath}/test/dest"
    rm -rf "${folderPath}/test/src"
    (
        cd "${folderPath}/test" || exit 2
        tar -zxf src.tar.gz
    )
}

run_test() {
    # python backup.py -f testConfig.yml
    "$PY" backup.py -an -f "$testConfigFile" >> "$logFile"

    mv "${folderPath}/test/src/test/World_old.txt" "${folderPath}/test/src/test/World_new.txt"
    echo Updated > "${folderPath}/test/src/test/Hello.txt"

    mv "${folderPath}/test/src/test_multiPath/test/World_old.txt" "${folderPath}/test/src/test_multiPath/test/World_new.txt"
    echo Updated > "${folderPath}/test/src/test_multiPath/test/Hello.txt"

    mv "${folderPath}/test/src/test_no-delete/Hello_old.txt" "${folderPath}/test/src/test_no-delete/Hello_new.txt"
    mv "${folderPath}/test/src/test_no-delete/World_old.txt" "${folderPath}/test/src/test_no-delete/World_new.txt"

    # python backup.py -f testConfig.yml
    "$PY" backup.py -an -f "$testConfigFile" >> "$logFile"
}

gen_output_file() {
    SEPARATOR="################################################################################"

    {
        echo $SEPARATOR
        find "${folderPath}"/test/dest/* | sed 's|tests/integration/test/dest/||g'

        echo $SEPARATOR
        echo "echo test/Hello.txt"
        cat "${folderPath}/test/dest/test/Hello.txt"

        echo $SEPARATOR
        echo "echo test_multiPath/test/Hello.txt"
        cat "${folderPath}/test/dest/test_multiPath/test/Hello.txt"
    } > $outputFileResult

    # echo $SEPARATOR > $outputFileResult
    # find "${folderPath}/test/dest/* >> $outputFileResult"

    # echo $SEPARATOR >> $outputFileResult
    # echo "test/Hello.txt" >> $outputFileResult
    # cat "${folderPath}/test/dest/test/Hello.txt >> $outputFileResult"

    # echo $SEPARATOR >> $outputFileResult
    # echo "test_multiPath/test/Hello.txt" >> $outputFileResult
    # cat "${folderPath}/test/dest/test_multiPath/test/Hello.txt >> $outputFileResult"
}

check_output_file() {
    if cmp -s $outputFileExpected $outputFileResult; then
        echo "Test OK"
        exit 0
    else
        echo "TEST FAILED!!!"
        exit 1
    fi
    # cmp -s $outputFileExpected $outputFileResult
    # if [ $? -ne 0 ]; then
    #     echo "TEST FAILED!!!"
    #     exit 1
    # else
    #     echo "Test OK"
    #     exit 0
    # fi
}

prepare_test
run_test
gen_output_file
check_output_file
