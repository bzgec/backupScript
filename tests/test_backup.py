import os
import shutil
import sys
import unittest
from io import StringIO
from unittest.mock import patch

from backup import Backup
from helperLib import helper


class TestBackup(unittest.TestCase):
    def test_cfgPrint(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_cfgPrint.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Music"
"""
        TEST_CONFIG_FILE_CFG = (
            "[{'destinationPath': '/media/USER/Elements/', "
            + "'mainPath': '/media/USER/DISK/', "
            + "'toBackup': "
            + "[{'commonPath': '', 'folders': [{'path': 'Music'}]}]}]\n"
        )

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)
        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)

        # Redirest stdout to variable
        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        backup.print_cfg()

        printOutput = new_stdout.getvalue()

        sys.stdout = old_stdout

        self.assertEqual(TEST_CONFIG_FILE_CFG, printOutput)

    def test_createFoldersToBackup(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_test_createFoldersToBackup.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Books"
        - path: "Projects"
          options:
            - exclude: ["*.pyc"]
            - no-delete
        - path: "Music"
          options:
            - no-delete
"""
        TEST_CONFIG_FILE_CFG = [
            {
                "src": "/media/USER/DISK/Books/",
                "dest": "/media/USER/Elements/Books/",
                "backup": False,
                "options": [],
            },
            {
                "src": "/media/USER/DISK/Projects/",
                "dest": "/media/USER/Elements/Projects/",
                "backup": False,
                "options": [{"exclude": ["*.pyc"]}, "no-delete"],
            },
            {
                "src": "/media/USER/DISK/Music/",
                "dest": "/media/USER/Elements/Music/",
                "backup": False,
                "options": ["no-delete"],
            },
        ]

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)
        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)

        foldersToBackup = backup.get_folders_to_backup()

        self.assertEqual(TEST_CONFIG_FILE_CFG, foldersToBackup)

    def test_backup_all_folders(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_test_backup_all_folders.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Books"
        - path: "Music"
"""
        TEST_CONFIG_FILE_CFG = [
            {
                "src": "/media/USER/DISK/Books/",
                "dest": "/media/USER/Elements/Books/",
                "backup": True,
                "options": [],
            },
            {
                "src": "/media/USER/DISK/Music/",
                "dest": "/media/USER/Elements/Music/",
                "backup": True,
                "options": [],
            },
        ]

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)
        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE, backup_all_folders=True)

        foldersToBackup = backup.get_folders_to_backup()

        self.assertEqual(TEST_CONFIG_FILE_CFG, foldersToBackup)

    @patch("sys.stdin", StringIO("n\ny\nn\ny\n"))
    def test_select_folders_to_backup(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_test_select_folders_to_backup.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Books"
        - path: "Projects"
        - path: "Pictures"
        - path: "Music"
"""
        TEST_CONFIG_FILE_CFG = [
            {
                "src": "/media/USER/DISK/Books/",
                "dest": "/media/USER/Elements/Books/",
                "backup": False,
                "options": [],
            },
            {
                "src": "/media/USER/DISK/Projects/",
                "dest": "/media/USER/Elements/Projects/",
                "backup": True,
                "options": [],
            },
            {
                "src": "/media/USER/DISK/Pictures/",
                "dest": "/media/USER/Elements/Pictures/",
                "backup": False,
                "options": [],
            },
            {
                "src": "/media/USER/DISK/Music/",
                "dest": "/media/USER/Elements/Music/",
                "backup": True,
                "options": [],
            },
        ]

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)
        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        backup.select_folders_to_backup()

        sys.stdout = old_stdout

        foldersToBackup = backup.get_folders_to_backup()

        self.assertEqual(TEST_CONFIG_FILE_CFG, foldersToBackup)

    def test_prepRsyncCmd_dry_run(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_prepRsyncCmd_dry_run.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Music"
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)
        foldersToBackup = backup.get_folders_to_backup()
        self.assertEqual(
            [
                "rsync",
                "-n",
                "--delete",
                "-rltgoDv",
                "--modify-window=1",
                "/media/USER/DISK/Music/",
                "/media/USER/Elements/Music/",
            ],
            backup.prepare_rsync_cmd(foldersToBackup[0], dry_run=True),
        )

    def test_prepRsyncCmd_run(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_prepRsyncCmd_run.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Music"
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)
        foldersToBackup = backup.get_folders_to_backup()
        self.assertEqual(
            [
                "rsync",
                "--delete",
                "-rltgoDv",
                "--modify-window=1",
                "/media/USER/DISK/Music/",
                "/media/USER/Elements/Music/",
            ],
            backup.prepare_rsync_cmd(foldersToBackup[0], dry_run=False),
        )

    def test_prepRsyncCmd_noDelete(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_prepRsyncCmd_noDelete.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Music"
          options:
            - no-delete
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)
        foldersToBackup = backup.get_folders_to_backup()
        self.assertEqual(
            [
                "rsync",
                "-rltgoDv",
                "--modify-window=1",
                "/media/USER/DISK/Music/",
                "/media/USER/Elements/Music/",
            ],
            backup.prepare_rsync_cmd(foldersToBackup[0], dry_run=False),
        )

    def test_prepRsyncCmd_exclude(self):
        TEST_CONFIG_FILE = "./tests/outputs/testCfg_prepRsyncCmd_exclude.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "/media/USER/Elements/"  # External disk path - this is where to backup
  mainPath: "/media/USER/DISK/"
  toBackup:
    - commonPath: ""
      folders:
        - path: "Music"
          options:
            - exclude: ['*.pyc', '.venv*']
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)
        foldersToBackup = backup.get_folders_to_backup()
        self.assertEqual(
            [
                "rsync",
                "--delete",
                "--exclude=*.pyc",
                "--exclude=.venv*",
                "-rltgoDv",
                "--modify-window=1",
                "/media/USER/DISK/Music/",
                "/media/USER/Elements/Music/",
            ],
            backup.prepare_rsync_cmd(foldersToBackup[0], dry_run=False),
        )

    def test_create_parent_folders_abs(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_create_parent_folders_abs.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "tests/outputs/test_create_parent_folders/abs/"
  mainPath: "Res"
  toBackup:
    - commonPath: "cmnPath/"
      folders:
        - path: "tests"
"""
        FOLDER_PATH = "tests/outputs/test_create_parent_folders/abs/cmnPath/"

        try:
            shutil.rmtree("tests/outputs/test_create_parent_folders/abs")
        except FileNotFoundError:
            pass

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)
        backup.create_parent_folders()
        self.assertEqual(True, os.path.isdir(FOLDER_PATH))

    def test_create_parent_folders_rel(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_create_parent_folders_rel.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/outputs/test_create_parent_folders/rel/"
  mainPath: "Res"
  toBackup:
    - commonPath: "cmnPath/"
      folders:
        - path: "tests"
"""
        FOLDER_PATH = "./tests/outputs/test_create_parent_folders/rel/cmnPath/"

        try:
            shutil.rmtree("tests/outputs/test_create_parent_folders/rel")
        except FileNotFoundError:
            pass

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)
        backup.create_parent_folders()
        self.assertEqual(True, os.path.isdir(FOLDER_PATH))

    def test_check_if_diff_diff(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_check_if_diff_diff.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/outputs/test_backup/"
  mainPath: "./tests/test_backup/"
  toBackup:
    - commonPath: "simple/"
      folders:
        - path: "other"
"""
        noDiffOutputStr = """sending incremental file list
test_backup.py

sent 3,689 bytes  received 72 bytes  7,522.00 bytes/sec
total size is 802,654  speedup is 213.42 (DRY RUN)
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)

        self.assertEqual(True, backup.check_if_diff(noDiffOutputStr))

    def test_check_if_diff_noDiff(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_check_if_diff_noDiff.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/outputs/test_backup/"
  mainPath: "./tests/test_backup/"
  toBackup:
    - commonPath: "simple/"
      folders:
        - path: "other"
"""
        noDiffOutputStr = """sending incremental file list

sent 3,602 bytes  received 59 bytes  7,322.00 bytes/sec
total size is 785,890  speedup is 214.67
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(backup_cfg_file=TEST_CONFIG_FILE)

        self.assertEqual(False, backup.check_if_diff(noDiffOutputStr))

    def test_backup_simple(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_backup_simple.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/outputs/test_backup/"
  mainPath: "./tests/test_backup/"
  toBackup:
    - commonPath: "simple/"
      folders:
        - path: "other"
"""

        try:
            shutil.rmtree("tests/outputs/test_backup/simple")
        except FileNotFoundError:
            pass

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(
            backup_cfg_file=TEST_CONFIG_FILE,
            backup_all_folders=True,
            no_confirm_rsync=True,
        )

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        backup.backup()

        sys.stdout = old_stdout

        self.assertEqual(
            True, os.path.isfile("tests/outputs/test_backup/simple/other/simple.txt")
        )

    def test_backup_noDiff(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_backup_noDiff.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/test_backup/noDiff/dest/"
  mainPath: "./tests/test_backup/"
  toBackup:
    - commonPath: "noDiff/"
      folders:
        - path: "src"
"""

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(
            backup_cfg_file=TEST_CONFIG_FILE,
            backup_all_folders=True,
            no_confirm_rsync=True,
        )

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        backup.backup()

        printOutput = new_stdout.getvalue()

        sys.stdout = old_stdout

        testOk = False
        if ("No difference" in printOutput) and (not ("rsync" in printOutput)):
            testOk = True

        self.assertEqual(True, testOk)

    def test_backup_space(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_backup_space.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/outputs/test_backup/"
  mainPath: "./tests/test_backup/"
  toBackup:
    - commonPath: "space/"
      folders:
        - path: "ot her"
"""

        try:
            shutil.rmtree("tests/outputs/test_backup/space")
        except FileNotFoundError:
            pass

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(
            backup_cfg_file=TEST_CONFIG_FILE,
            backup_all_folders=True,
            no_confirm_rsync=True,
        )

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        backup.backup()

        sys.stdout = old_stdout

        self.assertEqual(
            True, os.path.isfile("tests/outputs/test_backup/space/ot her/space.txt")
        )

    def test_backup_err(self):
        TEST_CONFIG_FILE = "./tests/outputs/test_backup_err.yml"
        TEST_CONFIG_FILE_DATA = """
- destinationPath: "./tests/outputs/test_backup/"
  mainPath: "./tests/test_backup/"
  toBackup:
    - commonPath: "non-existent/"
      folders:
        - path: "err"
"""

        try:
            shutil.rmtree("tests/outputs/test_backup/non-existent")
        except FileNotFoundError:
            pass

        helper.setup_files(TEST_CONFIG_FILE)
        with open(TEST_CONFIG_FILE, "w") as f:
            f.write(TEST_CONFIG_FILE_DATA)

        backup = Backup(
            backup_cfg_file=TEST_CONFIG_FILE,
            backup_all_folders=True,
            no_confirm_rsync=True,
        )

        old_stdout = sys.stdout
        new_stdout = StringIO()
        sys.stdout = new_stdout

        returncode = backup.backup()

        sys.stdout = old_stdout

        self.assertEqual(
            23, returncode
        )  # 23 - rsync error: some files/attrs were not transferred


if __name__ == "__main__":
    unittest.main()
