#!/usr/bin/env python

# Standard libraries
import logging
import subprocess
import traceback

# 3rd party packages
import typer
from rich import print

# Local source
from helperLib import helper, import_config

DFLT_CFG_FILE = "backupConfig.yml"
DFLT_RSYNC_CMD = "rsync"


def main(
    backup_all: bool = typer.Option(
        False,
        "--backup-all",
        "-a",
        help="Backup all specified folders (don't ask which folders to backup).",
    ),
    cfg_file: str = typer.Option(
        "",
        "--config-file",
        "-f",
        help="Specify json file which stores information about which folders to backup.",
    ),
    no_confirm_rsync: bool = typer.Option(
        False, "--no-confirm", "-n", help="No need to confirm rsync command execution."
    ),
):

    helper.logger_setup(
        "backup.log", level=logging.INFO, date_format="%Y/%m/%d %H:%M:%S", file_mode="a"
    )

    if cfg_file == "":
        cfg_file = DFLT_CFG_FILE

    backup = Backup(backup_all, no_confirm_rsync, cfg_file)

    # backup.print_cfg()

    if backup_all is False:
        backup.select_folders_to_backup()

    backup.backup()


class Backup:
    """Backup class"""

    def __init__(
        self,
        backup_all_folders: bool = False,
        no_confirm_rsync: bool = False,
        backup_cfg_file: str = DFLT_CFG_FILE,
        rsync_cmd: str = DFLT_RSYNC_CMD,
    ):
        # Backup all folders command line option argument flag (-a, --all)
        self.backup_all_folders = backup_all_folders

        # No need to confirm before executing rsync command (-n, --no-confirm)
        self.no_confirm_rsync = no_confirm_rsync

        self.backup_cfg = import_config.get_yaml(backup_cfg_file)
        self.rsync_cmd = rsync_cmd

        self.create_folders_to_backup()

    def print_cfg(self) -> None:
        """Print configuration"""
        print(self.backup_cfg)

    def create_folders_to_backup(self) -> None:
        """Create folders backup"""
        self.folders_to_backup = []
        for disk_to_backup in self.backup_cfg:
            for disk_backup_cfg in disk_to_backup["toBackup"]:
                for folder in disk_backup_cfg["folders"]:
                    src = (
                        disk_to_backup["mainPath"]
                        + disk_backup_cfg["commonPath"]
                        + folder["path"]
                        + "/"
                    )
                    dest = (
                        disk_to_backup["destinationPath"]
                        + disk_backup_cfg["commonPath"]
                        + folder["path"]
                        + "/"
                    )
                    if "options" in folder:
                        options = folder["options"]
                    else:
                        options = []

                    self.folders_to_backup.append(
                        {
                            "src": src,
                            "dest": dest,
                            "backup": self.backup_all_folders,
                            "options": options,
                        }
                    )

    def get_folders_to_backup(self) -> list:
        """Get which folders to backup"""
        return self.folders_to_backup

    def select_folders_to_backup(self) -> None:
        """Interactively select which folders to backup"""
        print("Backup:")
        for folder in self.folders_to_backup:
            folder["backup"] = helper.prompt_yes_no(
                f"- {helper.bold_str(folder['src'])}?"
            )

    def prepare_rsync_cmd(self, folder: dict, dry_run: bool = True) -> list[str]:
        """Prepare rsync command"""
        path_src = folder["src"]
        path_dest = folder["dest"]
        cmd_arr = [self.rsync_cmd]

        if dry_run is True:
            cmd_arr.append("-n")

        cmd_arr.append("--delete")

        for option in folder["options"]:
            if option == "no-delete":
                cmd_arr.remove("--delete")
            elif "exclude" in option:
                for exclude_option in option["exclude"]:
                    cmd_arr.append(f"--exclude={exclude_option}")

        cmd_arr.append("-rltgoDv")
        cmd_arr.append("--modify-window=1")
        cmd_arr.append(path_src)
        cmd_arr.append(path_dest)
        return cmd_arr

    def create_parent_folders(self) -> None:
        """Create parent folders"""
        for disk_to_backup in self.backup_cfg:
            for disk_backup_cfg in disk_to_backup["toBackup"]:
                helper.setup_files(
                    disk_to_backup["destinationPath"]
                    + disk_backup_cfg["commonPath"]
                    + "/"
                )

    def check_if_diff(self, rsync_cmd_output: str) -> int:
        """
        Check if rsync dry run reports no difference (no need to copy files)
        Example output when there is no difference:
          sending incremental file list

          sent 3,602 bytes  received 59 bytes  7,322.00 bytes/sec
          total size is 785,890  speedup is 214.67
        """

        diff = True

        # There are exactly 5 lines returned if there is no difference
        if rsync_cmd_output.count("\n") == 4:
            diff = False

        return diff

    def backup(self) -> int:
        """Backup folders"""

        # Create all parent folders...
        self.create_parent_folders()

        returncode = 0

        try:
            for folder in self.folders_to_backup:
                if folder["backup"] is True:
                    print(f'[bold]Dry run for: {folder["src"]}[/bold]')
                    # Do a dry run
                    cmd = self.prepare_rsync_cmd(folder, dry_run=True)
                    logging.info(cmd)

                    result = subprocess.run(cmd, check=True, capture_output=True)
                    logging.info(result.stdout.decode("utf-8"))

                    if self.check_if_diff(result.stdout.decode("utf-8")) is False:
                        # No need to run real rsync command
                        print("  No difference")
                        logging.info("  No difference")
                    else:
                        print(result.stdout.decode("utf-8"))

                        # There is difference -> copying is necessary
                        cmd = self.prepare_rsync_cmd(folder, dry_run=False)
                        logging.info(cmd)

                        if (self.no_confirm_rsync is True) or (
                            helper.prompt_yes_no(helper.bold_str(f"Execute: {cmd}?"))
                            is True
                        ):
                            logging.info("Execute")
                            result = subprocess.run(
                                cmd, check=True, capture_output=True
                            )
                            logging.info(result.stdout.decode("utf-8"))
                        else:
                            logging.info("Skip")

        except subprocess.CalledProcessError as e:
            traceback_str = traceback.format_exc()
            print(e.stderr.decode("utf-8"))
            print(traceback_str)
            returncode = e.returncode

        return returncode


if __name__ == "__main__":
    typer.run(main)
