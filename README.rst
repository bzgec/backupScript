=============
Backup script
=============

:Info: Python backup script which backs up specified folders with rsync command.
:Authors:
    Bzgec

.. contents:: Table of Contents
   :depth: 2

.. role:: bash(code)
   :language: bash


Info
====
This is python script used to backup folders to external hard drive.
It uses `rsync` command to check if files/folders need to be copied before the
actual copying.

.. NOTE::

   `rsync` command can also delete files!!!

   You must set "no-delete" option in configuration file if you don't want to
   delete files on backup disk/folder. See `config example <exampleBackupConfig.yml>`__.

Before actual copying/backup a *dry run* command is run which shows what a real command
is going to do.
Then you decide if you are going to execute *real run* command which will actually copy/backup
files.

The default `rsync` command used: :bash:`rsync -rltgoDvP --modify-window=1 --delete`.

For now only Linux systems are supported (don't know how it is on Windows and
`rsync` command).

.. WARNING::

   We are not responsible for any data loss.

Intentions
----------
- replace manual copying of important/special files
- make copying more efficient (copy only those files which have changed).

`Here <rsync.md>`__ you can check from some information about `rsync` command.


Usage
=====
- You must specify which folders/disks to backup. This information is stored in a
  configuration file.

  - `Configuration file`_

  - How to pass configuration file to the script:

    - create `backupConfig.yml` file in the same directory as `backup.py <backup.py>`__ script.
      This way you don't need to specify configuration file when you launch the script,
      so script can be launched like this: `python backup.py`

    - pass configuration file as an argument when you launch the script:

      - :bash:`python backup.py -f myBackupConfig.yml`
      - :bash:`python backup.py -f myBackupConfig`

- The script first runs a *dry run* which shows what a real command is going to do. This is used
  because that way we can cancel if we see that something would go wrong. So first a *dry run*
  command is run and than you get prompted if a real command should be run.

  .. image:: ./Res/demo.gif
     :alt: demo.gif

Script parameters
-----------------

::

  Usage: backup.py [OPTIONS]

  Options:
    -a, --backup-all                Backup all specified folders (don't ask
                                    which folders to backup).
    -f, --config-file TEXT          Specify json file which stores information
                                    about which folders to backup.
    -n, --no-confirm                No need to confirm rsync command execution.


Configuration file
==================
* `Example <exampleBackupConfig.yml>`__

Special "folders" options:
--------------------------

- ``exclude``: specify which files/folders/patterns to *not* include in the backup
- ``no-delete``: keep files/folders which don't exist on source path but exist on destination path
  (don't delete files on destination path, this is useful for folders like Pictures...)

File naming
===========

The problem can occur when `rsync` is trying to create directories in a NTFS partition
with illegal characters.
From Naming Conventions Files and folders must not include illegal characters.

::

  Use any character in the current code page for a name, including Unicode characters
  and characters in the extended character set (128–255), except for the following:

  The following reserved characters:
   > (less than)
   < (greater than)
   : (colon)
   " (double quote)
   / (forward slash)
   \ (backslash)
   | (vertical bar or pipe)
   ? (question mark)
   * (asterisk)


* `Reference <tps://unix.stackexchange.com/a/632731>`__

You can use ``rename``/``perl-rename`` command to help you with renaming:

- Remove ``?``

  - :bash:`perl-rename --verbose -n 's/\?//' *`
  - Run command in every subfolder: :bash:`find . -name '*' -execdir bash -c "perl-rename --verbose -n 's/\?//' *" \;`

- Remove ``:``

  - :bash:`perl-rename --verbose -n 's/://' *`
  - Run command in every subfolder: :bash:`find . -name '*' -execdir bash -c "perl-rename --verbose -n 's/://' *" \;`

Remove ``-n`` to allow ``rename`` command to actually make changes.

Development
===========

Before each commit:

- check source code for best coding standards: :bash:`make check`
- run simple test: :bash:`make test`

Or you can just run :bash:`make commit` which will run all the necessary commands before running
:bash:`git commit`. Note that you still need to add files to index (:bash:`git add`).

.. WARNING::

   If there are some errors fix them before committing!

Dependencies
------------

- `GNU - make <https://www.gnu.org/software/make/>`__
- ``pip`` `requirements <quirements.dev.txt.dev.txt>`__


Suggestions
===========

If you have any suggestions please let me know (you can submit pull request, open a discussion or
open an issue).


License
=======

See the `LICENSE <LICENSE.md>`__ file for license rights and limitations (MIT).


TODO
====

- `[ ]` Add support for Windows
- `[ ]` Auto generate `Script parameters` section

Done
----
